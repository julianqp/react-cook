const Loader = () => {
  return (
    <div
      className={
        'my-3 flex mx-auto loader ease-linear animate-spin rounded-full border-8 border-t-8 border-gray-200 lg:h-40 lg:w-40 md:h-32 md:w-32 h-20 w-20'
      }
    ></div>
  );
};

export default Loader;
