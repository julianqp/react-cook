const Footer = () => {
  return (
    <footer className="inset-x-0 bottom-0 text-gray-700 body-font  bg-yellow-300">
      <div className="flex justify-center">
        <p className="text-sm text-gray-500 my-4">
          © {new Date().getFullYear()} Julián Querol Polo
        </p>
      </div>
    </footer>
  );
};

export default Footer;
//
