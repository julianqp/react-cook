const Titulo = ({ title, subtitle }) => {
  return (
    <div className="my-2 mt-8">
      <h1 className="text-3xl text-center">{title}</h1>
      <div className="my-3 px-4 text-center">
        <p>{subtitle}</p>
      </div>
    </div>
  );
};

export default Titulo;
