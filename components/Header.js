import Link from 'next/link';
import { useRouter } from 'next/router';
import { FaReact } from 'react-icons/fa';

const selected = ' text-blue-500';
const Header = () => {
  const { route } = useRouter();
  let option = 'recetas';
  if (route.includes('ingredientes')) option = 'ingredientes';
  return (
    <div className="flex flex-wrap items-center pt-2 sm:mb-2 divide-y divide-black sm:divide-none bg-yellow-300 py-2">
      <h1 className=" flex sm:w-1/4 w-full pl-2  text-2xl text-center sm:text-left">
        <FaReact className="mr-2 text-blue-700 my-auto" /> Cook
      </h1>
      <div className="sm:w-2/3 w-full flex justify-center sm:justify-start">
        <Link href="/">
          <a
            className={`px-2 py-1 ${
              option === 'recetas' ? ` ${selected} ` : ''
            }`}
          >
            Recetas
          </a>
        </Link>
        <Link href="/ingredientes">
          <a
            className={`px-2 py-1 ${
              option === 'ingredientes' ? ` ${selected} ` : ''
            }`}
          >
            Ingredientes
          </a>
        </Link>
      </div>
    </div>
  );
};

export default Header;
