const Ingredientes = ({ ingredients, removeIngredient }) => {
  return (
    <div className="container mx-auto">
      <div className="flex justify-center my-1">
        {ingredients.map(x => {
          return (
            <p
              className="px-2 mx-2 bg-blue-200 rounded-full flex items-center"
              key={x}
            >
              {x}
              <span
                onClick={async () => removeIngredient(x)}
                className="px-1 font-bold text-red-500 hover:text-red-900"
              >
                <svg
                  className="h-4"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              </span>
            </p>
          );
        })}
      </div>
    </div>
  );
};

export default Ingredientes;
