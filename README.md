# React Cook

Proyecto realizado con React js, Next js y taildwin css.

Para iniciar el proyecto:

- git clone git@bitbucket.org:julianqp/react-cook.git
- cd react-cook
- npm install
- npm run dev
- http://localhost:3000

A aprtir de ahí habrá dos url para navegar.

- http://localhost:3000 "Búsqueda de recetas"
- http://localhost:3000/ingredientes "Búsqueda de recetas por ingredientes"
