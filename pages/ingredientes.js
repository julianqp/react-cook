import { useState } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import config from '../config/config';
import Recetas from '../components/Recetas';
import GroupIngredients from '../components/GroupIngredients';
import axios from 'axios';
import Loader from '../components/Loader';
import Titulo from '../components/Titulo';

const Ingrediente = () => {
  const [loading, setLoading] = useState(false);
  const [ingredient, setIngredient] = useState('');
  const [ingredients, setIngredients] = useState([]);
  const [platos, setPlatos] = useState([]);
  const [more, setMore] = useState(true);

  const addIngredient = async () => {
    setLoading(true);
    const newIngredient = [...ingredients, ingredient];
    setIngredients(newIngredient);
    setIngredient('');
    await search(newIngredient);
    setLoading(false);
  };

  const removeIngredient = async elem => {
    const index = ingredients.indexOf(elem);
    const rmIngredients = [...ingredients];
    if (index > -1) {
      rmIngredients.splice(index, 1);
      setIngredients(rmIngredients);
      await search(rmIngredients);
    }
  };

  const searchMore = async () => {
    const newPage = page + 1;
    setPage(newPage);
    const url = `${config.URL}?q=${ingredients.join()}&p=${newPage}`;

    const response = await axios.post('https://julian-cors.herokuapp.com/', {
      url,
    });

    if (response.data.results) {
      const algo = platos.concat(response.data.results);
      setPlatos(algo);
    } else {
      setMore(false);
    }
  };

  const search = async newIngredient => {
    if (newIngredient.length > 0) {
      const ingradientsForSearch = newIngredient.join();

      const url = `${config.URL}?i=${ingradientsForSearch}&p=1`;

      const response = await axios.post('https://julian-cors.herokuapp.com/', {
        url,
      });

      if (response.data.results) {
        setPlatos(response.data.results);
      }
    } else {
      setPlatos([]);
    }
  };

  let content = null;

  if (loading) {
    content = <Loader />;
  } else if (platos.length > 0) {
    content = (
      <div>
        <Recetas platos={platos} />
        <div className="px-5 sm:px p-2 w-full flex justify-center">
          {more ? (
            <button
              onClick={async () => await searchMore()}
              className="py-1 px-2 text-base  rounded-md bg-blue-200 hover:bg-blue-300"
            >
              Ver más
            </button>
          ) : (
            <p>No hay mas Recetas</p>
          )}
        </div>
      </div>
    );
  }

  return (
    <div className="flex flex-col min-h-screen">
      <div className="sticky top-0 bg-white w-full">
        <Header />
      </div>
      <main className="container mx-auto flex-grow ">
        <Titulo
          title={'Búsqueda por Ingredientes'}
          subtitle="Escriba los ingredientes que desea, para obtener recetas que contengan los ingredientes"
        />
        <div className="flex flex-wrap justify-center items-end">
          <div className="mx-5 sm:px-0 py-2 sm:w-2/4 md:w-2/4 lg:w-2/5  w-full">
            <label htmlFor="search" className="leading-8 text-sm text-gray-700">
              Ingredientes
            </label>
            <input
              onKeyPress={async e => {
                if (e.code === 'Enter') {
                  await addIngredient(ingredient);
                }
              }}
              onChange={e => setIngredient(e.target.value)}
              value={ingredient}
              type="text"
              id="search"
              name="search"
              className="px-2 w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1"
            />
          </div>
          <div className="px-5 sm:px p-2 sm:w-1/3 w-full">
            <div>
              {ingredient.length === 0 ? (
                <button
                  disabled={true}
                  className="py-1 px-2 text-base  rounded-md bg-blue-300 disabled:opacity-50 cursor-not-allowed"
                >
                  Añadir
                </button>
              ) : (
                <button
                  onClick={async () => await addIngredient()}
                  className="py-1 px-2 text-base  rounded-md bg-blue-300 hover:bg-blue-400"
                >
                  Añadir
                </button>
              )}
            </div>
          </div>
        </div>{' '}
        {ingredients.length > 0 ? (
          <GroupIngredients
            removeIngredient={removeIngredient}
            ingredients={ingredients}
          />
        ) : null}
        {content}
      </main>
      <Footer />
    </div>
  );
};

export default Ingrediente;
